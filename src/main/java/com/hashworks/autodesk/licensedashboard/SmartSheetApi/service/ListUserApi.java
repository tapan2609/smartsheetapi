package com.hashworks.autodesk.licensedashboard.SmartSheetApi.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.hashworks.autodesk.licensedashboard.SmartSheetApi.model.ListUser;
import com.hashworks.autodesk.licensedashboard.SmartSheetApi.model.dto.ListUserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

@Service
@Slf4j
public class ListUserApi {

    @Value("${accessToken}")
    private String accessToken;

    @Value("${listuser.url}")
    private String listUserUrl;

    public Map listUsersService() throws IOException {
        int activeLicenseCount=0;
        int count=0;
        Map map = new HashMap();
        Map responseEntityResult = getUserData();
        List<LinkedHashMap<?,?>> linkedHashMapList = (List<LinkedHashMap<?, ?>>) responseEntityResult.get("data");
        for(int i=0;i<linkedHashMapList.size();i++)
        {
            if(linkedHashMapList.get(i).get("licensedSheetCreator").equals(true))
            {
                count++;
            }
            activeLicenseCount = count;
        }
        long inactiveLicenseCount = Long.parseLong(responseEntityResult.get("totalCount").toString())-activeLicenseCount;
        map.put("totalUserCount",responseEntityResult.get("totalCount"));
        map.put("activeLicenseCount",activeLicenseCount);
        map.put("inactiveLicenseCount",inactiveLicenseCount);
        map.put("userData",responseEntityResult.get("data"));
        return map;
    }

    private Map getUserData() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        String token = "Bearer "+accessToken;
        httpHeaders.set("Authorization ",token);
        System.out.println(httpHeaders);
        HttpEntity<String> repoUrl = new HttpEntity<>(httpHeaders);
        ResponseEntity<Map> result = restTemplate.exchange(listUserUrl, HttpMethod.GET,repoUrl,Map.class);
        return result.getBody();
    }
}
