package com.hashworks.autodesk.licensedashboard.SmartSheetApi.model.dto;

import lombok.Data;

@Data
public class ListUserDto {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private Boolean admin;
    private Boolean licensedSheetCreator;
    private Boolean groupAdmin;
    private Boolean resourceViewer;
    private String status;
    private String name;
    private String profileImage;
    private int sheetCount;
}
