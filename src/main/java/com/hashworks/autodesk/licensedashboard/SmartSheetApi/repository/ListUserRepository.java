package com.hashworks.autodesk.licensedashboard.SmartSheetApi.repository;

import com.hashworks.autodesk.licensedashboard.SmartSheetApi.model.ListUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListUserRepository extends JpaRepository<ListUser,Long> {

}
