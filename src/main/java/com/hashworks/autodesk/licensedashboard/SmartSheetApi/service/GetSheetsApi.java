package com.hashworks.autodesk.licensedashboard.SmartSheetApi.service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.naming.AuthenticationException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Service
public class GetSheetsApi {

    @Value("${accessToken}")
    private String accessToken;

    @Value("${smartsheet.url}")
    private String smartSheetUrl;

    @Value("${listuser.url}")
    private String listUserUrl;

    public Map getSheets()
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        String token = "Bearer "+accessToken;
        httpHeaders.set("Authorization ",token);
        System.out.println(httpHeaders);
        HttpEntity<String> repoUrl = new HttpEntity<>(httpHeaders);
        ResponseEntity<Map> result = restTemplate.exchange(smartSheetUrl, HttpMethod.GET,repoUrl,Map.class);
        Map responseEntityResult=result.getBody();
        return responseEntityResult;
    }

}
