package com.hashworks.autodesk.licensedashboard.SmartSheetApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartSheetApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartSheetApiApplication.class, args);
	}
}
