package com.hashworks.autodesk.licensedashboard.SmartSheetApi.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name="user_details")
public class ListUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private Boolean admin;
    private Boolean licensedSheetCreator;
    private Boolean groupAdmin;
    private Boolean resourceViewer;
    private String status;
    private int sheetCount;
    private String name;

    @Column(name="userId")
    private String userId;

    @Column(columnDefinition = "LONGBLOB",name="profileImage")
    private String profileImage;
}
