package com.hashworks.autodesk.licensedashboard.SmartSheetApi.controller;

import com.hashworks.autodesk.licensedashboard.SmartSheetApi.service.GetSheetsApi;
import com.hashworks.autodesk.licensedashboard.SmartSheetApi.service.ListUserApi;
import com.hashworks.autodesk.licensedashboard.SmartSheetApi.service.ReadWriteSheetApi;
import com.hashworks.autodesk.licensedashboard.SmartSheetApi.utility.ResponseUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/smartsheet")
public class ReadWriteController {

    @Autowired
    private ResponseUtility responseJsonUtil;

    @Autowired
    private ReadWriteSheetApi readWriteSheetApi;

    @Autowired
    private ListUserApi listUserApi;


    @Autowired
    private GetSheetsApi getSheetsApi;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<?> listCloudProviders() throws Exception {
        try {
            Map responseMap = new HashMap();
            HttpStatus status = HttpStatus.OK;
            responseMap = responseJsonUtil.getResponseJson(200,readWriteSheetApi.ReadWriteSheetApi());
            return new ResponseEntity<Object>(responseMap, HttpStatus.OK);
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    @RequestMapping(value = "sheets", method = RequestMethod.GET)
    public ResponseEntity<?> getSheet() throws Exception {
            Map responseMap = new HashMap();
            HttpStatus status = HttpStatus.OK;
            responseMap = responseJsonUtil.getResponseJson(200,getSheetsApi.getSheets());
            return new ResponseEntity<Object>(responseMap, HttpStatus.OK);

    }

    @RequestMapping(value = "listUsers", method = RequestMethod.GET)
    public ResponseEntity<?> listUser() throws Exception {
        Map responseMap = new HashMap();
        HttpStatus status = HttpStatus.OK;
        responseMap = responseJsonUtil.getResponseJson(200,listUserApi.listUsersService());
        return new ResponseEntity<Object>(responseMap, HttpStatus.OK);

    }
}
