package com.hashworks.autodesk.licensedashboard.SmartSheetApi.service;

import com.smartsheet.api.Smartsheet;
import com.smartsheet.api.SmartsheetException;
import com.smartsheet.api.SmartsheetFactory;
import com.smartsheet.api.models.Cell;
import com.smartsheet.api.models.Column;
import com.smartsheet.api.models.Row;
import com.smartsheet.api.models.Sheet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class ReadWriteSheetApi {

    @Value("${accessToken}")
    private String accessToken;

    static {
        // These lines enable logging to the console
        System.setProperty("Smartsheet.trace.parts", "RequestBodySummary,ResponseBodySummary");
        System.setProperty("Smartsheet.trace.pretty", "true");
    }

    private static HashMap<String, Long> columnMap = new HashMap<String, Long>();



    public String ReadWriteSheetApi() throws SmartsheetException {

        try {
            // Initialize client
            Smartsheet ss = SmartsheetFactory.createDefaultClient(accessToken);

            Sheet sheet = ss.sheetResources().importXlsx("SampleSheet.xlsx", "sample", 0, 0);

            // Load the entire sheet
            sheet = ss.sheetResources().getSheet(sheet.getId(), null, null, null, null, null, null, null);
            System.out.println("Loaded " + sheet.getRows().size() + " rows from sheet: " + sheet.getName());

            // Build the column map for later reference
            for (Column column : sheet.getColumns())
                columnMap.put(column.getTitle(), column.getId());

            // Accumulate rows needing update here
            ArrayList<Row> rowsToUpdate = new ArrayList<Row>();

            for (Row row : sheet.getRows()) {
                Row rowToUpdate = evaluateRowAndBuildUpdates(row);
                if (rowToUpdate != null)
                    rowsToUpdate.add(rowToUpdate);
            }

            if (rowsToUpdate.isEmpty()) {
                System.out.println("No updates required");
            } else {
                // Finally, write all updated cells back to Smartsheet
                System.out.println("Writing " + rowsToUpdate.size() + " rows back to sheet id " + sheet.getId());
                ss.sheetResources().rowResources().updateRows(sheet.getId(), rowsToUpdate);
                System.out.println("Done");
            }

        }

        catch (Exception ex) {
            System.out.println("Exception : " + ex.getMessage());
            ex.printStackTrace();
        }

        return "Smart Sheet Api, Read write functionality tested";
    }

    private Row evaluateRowAndBuildUpdates(Row sourceRow) {
        Row rowToUpdate = null;
        // Find cell we want to examine
        Cell statusCell = getCellByColumnName(sourceRow, "Status");
        if ("Complete".equals(statusCell.getDisplayValue())) {
            Cell remainingCell = getCellByColumnName(sourceRow, "Remaining");
            if (! "0".equals(remainingCell.getDisplayValue()))                  // Skip if "Remaining" is already zero
            {
                System.out.println("Need to update row #" + sourceRow.getRowNumber());

                Cell cellToUpdate = new Cell();
                cellToUpdate.setColumnId(columnMap.get("Remaining"));
                cellToUpdate.setValue(0);

                List<Cell> cellsToUpdate = Arrays.asList(cellToUpdate);

                rowToUpdate = new Row();
                rowToUpdate.setId(sourceRow.getId());
                rowToUpdate.setCells(cellsToUpdate);
            }
        }
        return rowToUpdate;
    }

    private Cell getCellByColumnName(Row sourceRow, String columnName) {

        Long colId = columnMap.get(columnName);

        return sourceRow.getCells().stream()
                .filter(cell -> colId.equals((Long) cell.getColumnId()))
                .findFirst()
                .orElse(null);

    }


}
