CREATE TABLE user_details (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  email varchar(100) NOT NULL,
  firstName varchar(100) NOT NULL,
  lastName varchar(100) NOT NULL,
  admin BIT NOT NULL,
  licensedSheetCreator BIT NOT NULL,
  groupAdmin BIT NOT NULL,
  resourceViewer BIT NOT NULL,
  status varchar(100) DEFAULT NULL,
  name varchar(100) DEFAULT NULL,
  sheetcount int DEFAULT NULL,
  userId bigint(20) NOT NULL,
  profileImage longblob NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
